package io.pavement.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import java.util.ArrayList;

public class TTTMain extends Activity {

    //Represents a board, and the user total moves
    public ArrayList<String> board = new ArrayList<String>();
    public ArrayList<Integer> userMoves = new ArrayList<Integer>();

    //tracks the total moves for end-game
    private int moves;

    //switch used for determining if user turn
    public boolean isUserTurn = true;

    //used for re-use in the blockOrWin method between columns and rows
    public boolean column = false;

    private final String EMPTY_SPACE = ":";

    //to pick to win first, then block a user.
    public int bestMove;
    public boolean bestMoveWinning;

    public String userGamepieceChoice;
    public String computerGamepiece;

    public TTTView mTTTView;

    //Game over variables. If gameOver = true, user cannot play anymore
    public boolean gameOver = false;
    public static String YOU_LOSE = "You Lose! Better luck next time?";
    public static String DRAW = "It's a tie!";
    public static String SPACE_IN_USE = "Already a piece there, try another spot";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTTTView = new TTTView(this);
        setContentView(mTTTView);
        mTTTView.requestFocus();

        moves=0;
        initializeBoard();

        userGamepieceChoice = "X";
        computerGamepiece = "O";
        bestMove = -1;
        bestMoveWinning = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState){

        outState.putStringArrayList("board", board);
        outState.putIntegerArrayList("userMoves", userMoves);

        outState.putInt("moves", moves);
        outState.putBoolean("isUserTurn", isUserTurn);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle bundle){
        super.onRestoreInstanceState(bundle);

        board = bundle.getStringArrayList("board");
        userMoves = bundle.getIntegerArrayList("userMoves");

        moves = bundle.getInt("moves");
        isUserTurn = bundle.getBoolean("isUserTurn");
    }

    /*     NON-GAMEPLAY FUNCTIONS  */
    //restarts activity if there is a loser
    public void gameOver(String response){
        Toast t = Toast.makeText(this.getBaseContext(), response, 2000);
        t.setGravity(Gravity.TOP | Gravity.CENTER, 0, mTTTView.PADDING);
        t.show();
        gameOver = true;
    }
    //resets the activity
    public void reset(){
        Intent i = new Intent(this.getBaseContext(), TTTMain.class);
        startActivity(i);
    }



    /*     FOR CONVERTING X AND Y VALUES TO INDEXES, AND VICE VERSA   */
    //Takes an x and y, turns them into an index on the board
    public int convertToIndex(int x, int y){
        return (y*3)+x;
    }

    //Gets the x and y coordinates of an index for the gamepieces from board
    public Posn getPositionOfIndex(int index){
        int x;
        int y;

        if(index < 3){
            x= index;
            y = 0;
        }
        else if (index < 6){
            x = index - 3;
            y = 1;
        }
        else{
            x = index - 6;
            y = 2;
        }
        return new Posn(x, y);
    }


    /*     FOR PLACING TILES (USER AND COMPUTER)   */
    //Main method for handling user gamepiece placement
    public void userPlaceTile(int index){

        String s = board.get(index);
        //sets the selected tile
        if(s==EMPTY_SPACE){
            s = userGamepieceChoice;
            board.set(index, s);
            userMoves.add(index);
            moves+=1;

            mTTTView.postInvalidate();
            if(moves==board.size()){
                gameOver(DRAW);
            }
            else{
                isUserTurn = false;
                computerPlaceTile();
            }
        }
        else{
            Toast t = Toast.makeText(this.getBaseContext(), SPACE_IN_USE, 2000);
            t.setGravity(Gravity.TOP | Gravity.CENTER, 0, mTTTView.PADDING);
            t.show();
        }
    }

    //Main method for computer placing a gamepiece somewhere
    public void computerPlaceTile(){
        if(userMoves.size()==1){
            int cache = userMoves.get(0);

            //if user places tile in a corner in the first move,
            //place computer piece in the middle
            if(cache!=4){
                board.set(4, computerGamepiece);
                mTTTView.postInvalidate();
                isUserTurn = true;
            }
            else{
                board.set(8, computerGamepiece);
                mTTTView.postInvalidate();
                isUserTurn = true;
            }

            moves +=1;
        }

        //Counter measures for one loophole in existing defensive code
        if(userMoves.size()==2){
            if(oppositeCorners()){
                board.set(1, computerGamepiece);
                mTTTView.postInvalidate();
                isUserTurn = true;
                moves+=1;
            }

            else if(isOddCase()){
                mTTTView.postInvalidate();
                moves+=1;
                isUserTurn = true;
            }
        }

        //checks for danger or winners
        rowCheck();
        columnCheck();
        diagonalCheck(0, 4, 8);

        //chooses best move, ex take the win
        if(bestMove>=0){
            board.set(bestMove, computerGamepiece);
            moves+=1;
            if(!isUserTurn && bestMoveWinning){
                gameOver(YOU_LOSE);
            }
            isUserTurn = true;
            bestMove = -1;
        }

        //Case where there is no danger, and tile can be placed anywhere
        if(!isUserTurn)
            if(!placeInClosestCorner()){
                for(int i = 0; i < board.size(); i++){
                    String s = board.get(i);
                    if(s.equals(EMPTY_SPACE)){
                        board.set(i, computerGamepiece);
                        moves+=1;
                        isUserTurn = true;
                        i = 100;
                    }
                }
            }
    }

    /*      CHECKING FUNCTIONS FOR COMPUTER/GAMEOVER   */
    //checks if a row is in danger or can take the win
    public void rowCheck(){
        int i;
        for(i = 0; i < 3; i++){
            int row = (3*i);

            String s = (board.get(row))+(board.get(row+1))+(board.get(row+2));

            column = false;
            blockUserOrWin(s, i);
        }
    }

    //checks if a column is in danger or can take the win
    public void columnCheck(){
        int i;
        for(i = 0; i < 3; i++){
            String s = (board.get(i))+ (board.get(i+3)) + board.get(i+6);

            column = true;
            blockUserOrWin(s, i);
        }
    }

    //checks diagonal if a blocking is needed
    public void diagonalCheck(int first, int second, int third){

        int i = 0;
        int indexOfBlank = -1;
        String s = (board.get(first))+(board.get(second))+(board.get(third));

        //builds a string of length 3, to be used to identify issues
        for(int index=0; index < 3; index++){
            String temp = String.valueOf(s.charAt(index));
            if(temp.equals(userGamepieceChoice))
                i+=1;
            else if(temp.equals(computerGamepiece))
                i-=1;
            else
                indexOfBlank = index;
        }

        //if there are two of one piece, block or win accordingly
        if(i==2 || i<=-2){
            if(indexOfBlank==2)
                indexOfBlank = third;
            else if(indexOfBlank==0)
                indexOfBlank = first;
            else
                indexOfBlank = second;

            if(!isUserTurn && bestMoveWinning==false){
                bestMove = indexOfBlank;
                if(i<=-2)
                    bestMoveWinning = true;
                //board.set(indexOfBlank, computerGamepiece);
                // moves +=1;
                // mTTTView.postInvalidate();
            }


        }
        if(first==0 && gameOver==false)
            diagonalCheck(2,4,6);
    }

    //takes in a string with a column or row, and the type (either column or row)
    //Example of String s is X:X or XXO.
    public void blockUserOrWin(String s, int place){

        int i=0;
        int indexOfBlank=-1;

        //builds a string of length 3, to be used to identify issues
        for(int index=0; index < 3; index++){
            String temp = String.valueOf(s.charAt(index));
            if(temp.equals(userGamepieceChoice))
                i+=1;
            else if(temp.equals(computerGamepiece))
                i-=1;
            else
                indexOfBlank = index;
        }

        //if there are two of one piece, block or win accordingly
        if(i==2 || i <=-2){

            if (column){
                indexOfBlank = place + (indexOfBlank*3);
            }
            else
                indexOfBlank = indexOfBlank + (place*3);

            if(!isUserTurn && bestMoveWinning==false){
                bestMove = indexOfBlank;
                if(i<=-2)
                    bestMoveWinning = true;
            }

        }
    }

    /*    COMPUTER GAMEPIECE PLACEMENT HELPERS   */
    //places a gamepiece in the corner for a computer
    public boolean placeInClosestCorner(){
        boolean placed=false;
        int i=0;
        do{
            String s = board.get(i);
            if(!board.get(5).contains(EMPTY_SPACE) && sumOfUserMoves()>10
                    &board.get(8).contains(EMPTY_SPACE)){
                board.set(8, computerGamepiece);
                moves +=1;
                isUserTurn = true;
                placed = true;
            }
            else if(s.equals(EMPTY_SPACE)){
                board.set(i, computerGamepiece);
                moves +=1;
                isUserTurn = true;
                placed = true;
            }

            if(i==2)
                i=6;
            else
                i+=2;
        }while(placed==false && i <= 8);

        return placed;
    }

    //to determine if user is heavy in the bottom region of the gameboard
    public int sumOfUserMoves(){
        int placeholder=0;

        for (int i = 0; i < userMoves.size(); i++){
            placeholder += userMoves.get(i);
        }

        return placeholder;

    }

    //returns true if opposite corners are in play for user
    public boolean oppositeCorners(){
        int first = userMoves.get(0);
        int second = userMoves.get(1);

        return ((first+second==8) && (first%2==0 && second%2==0));

    }

    //there's one loophole :(
    public boolean isOddCase(){
        int first = userMoves.get(0);
        int second = userMoves.get(1);

        if((first==7 || second==7) && ((first==0 || second==2) || (first==2 || second==0))){
            if(first==2 || second==2)
                board.set(8, computerGamepiece);
            else
                board.set(6, computerGamepiece);

            return true;
        }
        else
            return false;
    }

    //initializes board
    public void initializeBoard(){
        for(int i = 0; i < 9; i ++){
            board.add(EMPTY_SPACE);
        }
    }
}