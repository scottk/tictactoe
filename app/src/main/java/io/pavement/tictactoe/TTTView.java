package io.pavement.tictactoe;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.nio.Buffer;

/**
 * Created by MACMINI on 2/24/14.
 */
public class TTTView extends View{

    //constants for placement of items
    private static int BLOCK_SIDES;
    private static int AVAILABLE_SPACE;
    private static int BLOCK_SPACING;
    public static int PADDING;
    private static int BUFFER_TOP;

    private static final String VIEW_STATE = "viewState";

    private int ID = (123);
    private final TTTMain mTTTMain;

    //To log reset bounds for button use
    private int resetLeftSide;
    private int resetRightSide;
    private int resetTop;
    private int resetBottom;

    //to log the bounds of the board
    private int boardBottom;
    private int boardTop;
    private int boardLeft;
    private int boardRight;

    private int width;
    private int height;

    private int xCoord;
    private int yCoord;

    Drawable gameTile;

    public TTTView(Context context) {
        super(context);
        mTTTMain = (TTTMain) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        setId(ID);

        gameTile = getResources().getDrawable(R.drawable.block);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable p = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable(VIEW_STATE, p);
        return bundle;
    }

    @Override
    protected void onDraw(Canvas canvas){

        width = getWidth();
        height = getHeight();

        /*    INITIALIZE CONSTANTS WITH WIDTH  */
        BLOCK_SPACING = width/40;
        PADDING = width/15;
        AVAILABLE_SPACE = (width - (PADDING*2) - (BLOCK_SPACING*2));

        BLOCK_SIDES = AVAILABLE_SPACE/3;

        BUFFER_TOP = width/4;

        //Initialize reset button bounds
        resetLeftSide = (BLOCK_SIDES+BLOCK_SPACING);
        resetRightSide = (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)+PADDING;
        resetTop = ((PADDING + ((BLOCK_SIDES+BLOCK_SPACING)*3))+BUFFER_TOP)+PADDING;
        resetBottom = (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)*3+ BUFFER_TOP;

        //initialize the board bound limits for touch
        boardLeft = PADDING;
        boardRight = (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)*3;
        boardBottom = (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)*3+ BUFFER_TOP;
        boardTop = (PADDING + BUFFER_TOP);

        /*   DRAWS BACKGROUND   */
        Paint background = new Paint();
        background.setColor(getResources().getColor(R.color.background_color));
        canvas.drawRect(0, 0, width, height, background);

        /*     DRAWS GAME TILES     */
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                gameTile.setBounds(PADDING + (BLOCK_SIDES+BLOCK_SPACING)*j, (PADDING + ((BLOCK_SIDES+BLOCK_SPACING)*i))+BUFFER_TOP,
                        (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)*j, (BLOCK_SIDES+PADDING)+(BLOCK_SIDES+BLOCK_SPACING)*i+ BUFFER_TOP);

                gameTile.draw(canvas);
            }
        }

        /*    DRAWS RESET BUTTON   */
        gameTile.setBounds(resetLeftSide, resetTop, resetRightSide, resetBottom);
        gameTile.draw(canvas);


        /*    SETS PAINT FOR GAMEPIECES (X or O)  */
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(getResources().getColor(
                R.color.letter_color));
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(BLOCK_SIDES * 0.5f);
        textPaint.setTextScaleX(BLOCK_SIDES / BLOCK_SIDES);
        textPaint.setTextAlign(Paint.Align.CENTER);

        /*    DRAWS RESET BUTTON TEXT   */
        canvas.drawText("Reset", ((BLOCK_SIDES+BLOCK_SPACING)*2)-BLOCK_SPACING*4,
                (BLOCK_SPACING/2)+((PADDING + ((BLOCK_SIDES+BLOCK_SPACING)*3))+BUFFER_TOP)+PADDING*3, textPaint);

        textPaint.setColor(getResources().getColor(
                R.color.letter_color));
        textPaint.setTextSize(BLOCK_SIDES * 0.75f);
        textPaint.setTextScaleX(BLOCK_SIDES / BLOCK_SIDES);

        /*     DRAWS CURRENT GAMEPIECES     */
        for(int i = 0; i < mTTTMain.board.size(); i++){

            String s = mTTTMain.board.get(i);
            Posn p = mTTTMain.getPositionOfIndex(i);

            if(s!=":")
                canvas.drawText(s, ((BLOCK_SIDES+BLOCK_SPACING)*p.x)+BUFFER_TOP-PADDING, (BLOCK_SIDES)+(BLOCK_SIDES+BLOCK_SPACING)*p.y+ BUFFER_TOP, textPaint);
        }


    }

    @Override
    public boolean onTouchEvent(final MotionEvent e){


        switch(e.getActionMasked()){
            case MotionEvent.ACTION_DOWN:

                //round to get simple x and y coordinates, 0 through 2 that correspond with the
                //row of 3
                xCoord = (Math.round(e.getX() / BLOCK_SIDES))-1;
                yCoord = (Math.round(e.getY() / BLOCK_SIDES))-2;

                //if reset is pressed, restart activity
                if(resetButtonPressed(e.getX(), e.getY())){
                    mTTTMain.reset();
                }

                //Do not allow user to place pieces unless these criteria are met
                else if(!mTTTMain.gameOver && mTTTMain.isUserTurn){
                    if(onBoard(e.getX(), e.getY())){
                        //to catch rounding errors
                        int catchIndex = mTTTMain.convertToIndex(xCoord, yCoord);
                        if(catchIndex>=0 && catchIndex <=mTTTMain.board.size())
                            mTTTMain.userPlaceTile(catchIndex);
                    }
                }
        }
        return true;
    }

    /*        TOUCH LIMITIATION HELPER METHODS   */
    //determines if reset button pressed
    public boolean resetButtonPressed(float x, float y){
        return (x > resetLeftSide && x < resetRightSide && y > resetTop && y < resetBottom);
    }

    //determines if touch is on the tic tac toe board or not
    public boolean onBoard(float x, float y){
        return (x > boardLeft && x < boardRight && y > boardTop && y < boardBottom);
    }
}
